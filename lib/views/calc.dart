import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_calculator/bloc/history_bloc.dart';

import 'package:flutter_calculator/views/history.dart';

class CalculatorPage extends StatefulWidget {
  CalculatorPage({Key key, this.title}) : super(key: key);

  final title;

  @override createState() => _CalculatorPageState();
}

class _CalculatorPageState extends State<CalculatorPage> {

  var _var1 = '';
  var _var2 = '';
  var _sign = '';
  var _displayText = '';
  var _eqResult = '';

  @override build(context) => Scaffold(
    appBar: AppBar(
      title: Text(widget.title),
      actions: [
        PopupMenuButton(
          icon: Icon(Icons.menu),
          onSelected: (item) { switch (item) {
            case 'History':
              Navigator.push(context, MaterialPageRoute(builder: (ctx) => HistoryPage()));
              break;
            case 'Settings':
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Not Yet Implemented'),),);
              break;
          } },
          itemBuilder: (ctx) => [
            PopupMenuItem(value: 'History', child: Text('History'),),
            PopupMenuItem(value: 'Settings', child: Text('Settings'),),
          ],
        )
      ],
    ),
    body: Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        _resultText(),
        _line(),
        _actionButtonRow(['7', '8', '9', '+', 'C']),
        _actionButtonRow(['4', '5', '6', '-', 'CE']),
        _actionButtonRow(['1', '2', '3', '*', '']),
        _actionButtonRow(['.', '0', '', '/', '=']),
      ],
    ),
  );

  _line() => SizedBox(height: 1, child: Container(color: Colors.black38));

  _resultText() => SizedBox(
    width: MediaQuery.of(context).size.width,
    child: Padding(padding: EdgeInsets.all(20), child: Text(
      _displayText,
      textAlign: TextAlign.right,
      style: TextStyle(fontSize: 32,),
    ),),
  );

  _actionButtonRow(labels) => Row(children: labels.map<Widget>((l) => _actionButton(l) as Widget).toList());

  _actionButton(label) => SizedBox(
    width: MediaQuery.of(context).size.width / 5,
    height: MediaQuery.of(context).size.height / 10,
    child: FlatButton(
      child: Text(label, style: TextStyle(fontSize: 32,),),
      onPressed: () => _actionButtonPressed(label),
      hoverColor: _actionButtonSplash(label),
      highlightColor: _actionButtonSplash(label),
      splashColor: _actionButtonSplash(label),
    ),
  );

  _actionButtonSplash(label) => label.isEmpty ? Colors.transparent : Colors.black26;

  _actionButtonPressed(label) => setState(() {
    if (_eqResult.isNotEmpty) _clear();
    if (int.tryParse(label) != null) {
      if (_sign.isEmpty) _var1 += label;
      else _var2 += label;
    } else if (label == '.') {
      if (_sign.isEmpty && !_var1.contains('.')) _var1 += label;
      else if (_sign.isNotEmpty && !_var2.contains('.')) _var2 += label;
    } else if (_var1.isNotEmpty && _var2.isEmpty && ['+', '-', '*', '/'].contains(label)) {
      _sign = label;
    }
    if (_eqResult.isEmpty) {
      if (label == 'CE') _clearEntry();
      else if (label == 'C') _clear();
      else if (label == '=') _solve();
    }
    _buildDisplayText();
  });

  _clearEntry() {
    if (_var2.isNotEmpty) _var2 = _var2.substring(0, _var2.length - 1);
    else if (_sign.isNotEmpty) _sign = '';
    else if (_var1.isNotEmpty) _var1 = _var1.substring(0, _var1.length - 1);
  }

  _clear() {
    _var1 = '';
    _var2 = '';
    _sign = '';
    _eqResult = '';
    _displayText = '';
  }

  _solve() {
    if (_sign == '+') _eqResult = (double.parse(_var1) + double.parse(_var2)).toString();
    else if (_sign == '-') _eqResult = (double.parse(_var1) - double.parse(_var2)).toString();
    else if (_sign == '*') _eqResult = (double.parse(_var1) * double.parse(_var2)).toString();
    else if (_sign == '/') {
      if (double.parse(_var2) == 0.0) _eqResult = 'Division by 0 is not supported.';
      else _eqResult = (double.parse(_var1) / double.parse(_var2)).toString();
    }
    BlocProvider.of<HistoryBloc>(context)..add(UpdateHistoryEvent('$_var1 $_sign $_var2 = $_eqResult'));
  }

  _buildDisplayText() {
    var text = '';
    if (_var1.isNotEmpty) text += _var1;
    if (_sign.isNotEmpty) text += '\n$_sign';
    if (_var2.isNotEmpty) text += '\n$_var2';
    if (_eqResult.isNotEmpty) text += '\n=\n$_eqResult';
    _displayText = text;
  }
}