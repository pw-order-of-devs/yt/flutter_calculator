import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_calculator/bloc/history_bloc.dart';

class HistoryPage extends StatefulWidget {

  @override createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {

  @override initState() {
    BlocProvider.of<HistoryBloc>(context)..add(GetHistoryEvent());
    super.initState();
  }

  @override build(context) => Scaffold(
    appBar: AppBar(title: Text('Flutter ${kIsWeb ? 'Web' : 'Mobile'} Calc History'),),
    body: BlocBuilder<HistoryBloc, HistoryState>(
      builder: (context, state) {
        if (state is GetHistory) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: state.history.map<Widget>((h) => Padding(
              padding: EdgeInsets.all(10),
              child: Text(h, style: TextStyle(fontSize: 26),),
            ),).toList(),
          );
        } else return Column();
      },
    ),
  );
}