import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_calculator/bloc/history_bloc.dart';
import 'package:flutter_calculator/bloc/theme_bloc.dart';
import 'package:flutter_calculator/views/calc.dart';

main() => runApp(MultiBlocProvider(
  child: CalculatorApp(),
  providers: [
    BlocProvider<ThemeBloc>(create: (context) => ThemeBloc()..add(SetGreenTheme(),),),
    BlocProvider<HistoryBloc>(create: (context) => HistoryBloc(),),
  ],
));

class CalculatorApp extends StatelessWidget {

  @override build(context) => BlocBuilder<ThemeBloc, ThemeState>(
    builder: (context, state) => MaterialApp(
      title: 'Flutter Demo',
      theme: state.themeData,
      home: CalculatorPage(title: 'Flutter ${kIsWeb ? 'Web' : 'Mobile'} Calculator'),
    ),
  );
}
