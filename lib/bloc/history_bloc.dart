import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class HistoryState extends Equatable {
  const HistoryState();

  @override get props => [];
}

class DefaultState extends HistoryState {}

class GetHistory extends HistoryState {
  final history;

  const GetHistory(this.history);

  @override get props => [history];
}

class UpdatedHistory extends HistoryState {}

abstract class HistoryEvent extends Equatable {
  const HistoryEvent();

  @override get props => [];
}

class GetHistoryEvent extends HistoryEvent {}
class UpdateHistoryEvent extends HistoryEvent {
  final entry;

  const UpdateHistoryEvent(this.entry);

  @override get props => [entry];
}

class HistoryBloc extends Bloc<HistoryEvent, HistoryState> {
  var history = [];

  @override get initialState => DefaultState();

  @override mapEventToState(event) async* {
    if (event is GetHistoryEvent) {
      yield GetHistory(history);
    } else if (event is UpdateHistoryEvent) {
      history.add(event.entry);
      yield UpdatedHistory();
    }
  }
}
