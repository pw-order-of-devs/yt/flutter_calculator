import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class ThemeState extends Equatable {
  final ThemeData themeData;

  const ThemeState() : themeData = null;

  @override get props => [themeData];
}

class GreenTheme extends ThemeState {
  final ThemeData themeData;

  GreenTheme({themeData}) : themeData = ThemeData(primarySwatch: Colors.green,);

  @override get props => [themeData];
}

class YellowTheme extends ThemeState {
  final ThemeData themeData;

  YellowTheme({themeData}) : themeData = ThemeData(primarySwatch: Colors.yellow,);

  @override get props => [themeData];
}

abstract class ThemeEvent extends Equatable {
  const ThemeEvent();

  @override get props => [];
}

class SetGreenTheme extends ThemeEvent {}
class SetYellowTheme extends ThemeEvent {}

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {

  @override get initialState => GreenTheme();

  @override mapEventToState(event) async* {
    if (event is SetGreenTheme) { yield GreenTheme(); }
    else if (event is SetYellowTheme) { yield YellowTheme(); }
  }
}
